LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := ANXCamera
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := system/priv-app/ANXCamera/ANXCamera.apk
LOCAL_CERTIFICATE := platform
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_DEX_PREOPT := false
LOCAL_OVERRIDES_PACKAGES := Camera2 Snap SnapdragonCamera
include $(BUILD_PREBUILT)
