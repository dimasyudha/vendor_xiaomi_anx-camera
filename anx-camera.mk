VENDOR_PATH := vendor/xiaomi/anx-camera

PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,$(VENDOR_PATH)/system/etc,$(TARGET_COPY_OUT_SYSTEM)/etc) \
    $(call find-copy-subdir-files,*,$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm,$(TARGET_COPY_OUT_SYSTEM)/priv-app/ANXCamera/lib/arm) \
    $(call find-copy-subdir-files,*,$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64,$(TARGET_COPY_OUT_SYSTEM)/priv-app/ANXCamera/lib/arm64)

PRODUCT_PACKAGES += \
	ANXCamera
